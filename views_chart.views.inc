<?php
function views_chart_views_plugins(){
	return array(
		"style"=>array(
			"chart"=>array(
				"title"=>t("Chart"),
				"help"=>t("Display views data as a chart"),
				"handler"=>"views_chart_plugin_style",
				"theme"=>"views_chart",
				"uses row plugin"=>false,
				"uses row class"=>false,
				"uses grouping"=>true,
				"uses options"=>true,
				"uses fields"=>true,
				"type"=>"normal"
			)
		)
	);
}